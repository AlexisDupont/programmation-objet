#include <stdio.h>
#include <stdlib.h>
void echange (int *x, int *y){
int res;
res=*x;
*x=*y;
*y=res;
}

int main()
{
    int x, y;
    printf ("Quelle est la valeur de x \n");
    scanf ("%d", &x);
    printf ("Quelle est la valeur de y \n");
    scanf ("%d", &y);
    echange (&x,&y);
    printf("x = %d et y = %d ", x, y);
    return 0;
}
