#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *saisie()
{
    char tab[1000], *p;
    int taille;
    gets(tab);
       taille=strlen(tab);
       p=(char*)malloc((taille+1)*sizeof(char));
       strcpy(p,tab);
       return p;
}


int ftaille(char *tab, char caractere)
{
    int i;
    int occurence;
    occurence=0;
    for (i=0;tab[i]!='\0';i++)
    {
        if (tab[i] == caractere)
        {
            occurence++;
        }
    }
    return occurence;
}


void copied(char *tab, char*tab2,char caractere)
{
    int i=0,j=0;

    while(tab[i] != '\0')
    {
        tab2[j++]=tab[i];
        if (tab[i] == caractere)
        {
             tab2[j]=tab[i];
             j++;
        }
        i++;
    }
    tab2[j]='\0';
}

void copies(char *tab, char*tab2,char caractere)
{
    int i=0,j=0;

    while(tab[i] != '\0')
    {
        if (tab[i] != caractere)
        {
             tab2[j]=tab[i];
             j++;
        }
        i++;
    }
    tab2[j]='\0';
}

char *dedoubler(char *tab,char car)
{
    char *tab2;
    int taille,occ;
    occ = ftaille(tab,car);
    taille = strlen(tab) + occ + 1;
    tab2=(char*)malloc(taille*sizeof(char));
    copied(tab,tab2,car);
    free(tab) ;
    return tab2;
}

char *supprimer(char *tab,char car)
{
    char *tab2;
    int taille,occ;
    occ = ftaille(tab,car);
    taille = strlen(tab) - occ + 1;
    tab2=(char*)malloc(taille*sizeof(char));
    copies(tab,tab2,car);
    free(tab) ;
    return tab2;
}


/*int main()
{
    char *tab;
    char *tmp;
    char lettre;
    printf("entrer une chaine\n");
    tmp=saisie();
    printf("donnez une lettre a dedoubler : ");
    scanf("%c",&lettre);
    tab = dedouble(tmp,lettre);
    printf("chaine : %s\n",tab);

    return 0;
}
*/
