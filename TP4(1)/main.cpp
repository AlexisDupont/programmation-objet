#include <iostream>
#include <cstring>
#include "../TP3 ex5/main.c"
#include "../TP3 ex3/main.c"

using namespace std;

class Mystring
{
private :
    char *tab;
    int n, caractere_spe;
    int * stat;
    void majstat();
public :
    Mystring ();
    Mystring (char*);
    Mystring (int , char);
    Mystring& operator=(const Mystring&);
    Mystring operator+(const Mystring&);
    Mystring operator-(const Mystring&);
    friend ostream& operator<<(ostream&, Mystring&);
    ~Mystring();
    void affiche();
    void init();
    void supprime(char);
    void dedouble(char);
    void concat(char*, char*);
};
void Mystring::majstat()
{
    n=0;
    caractere_spe=0;
    for (int i=0;i<26;i++){
        stat[i]=0;
    }
    if (tab!=NULL){
        for (int i=0;tab[i]!='\0';i++){
            n++;
            if (tolower(tab[i])>='a' && tolower(tab[i]<='z')){
                stat[tolower(tab[i])-'a']++;
            }
            else{
                caractere_spe++;
            }
        }
    }
}

Mystring::Mystring()
{
    cout<<"constructeur 1"<<endl;
    tab = NULL;
    n = caractere_spe = 0;
    stat = new int [26];
    majstat();
}


Mystring::Mystring( char * par_chaine)
{
    n=strlen(par_chaine);
    tab= new char [n+1];
    cout<<"constructeur 2"<<endl;
    strcpy(tab,par_chaine);
    tab[n+1]='\0';
    stat = new int [26];
    majstat();
}

Mystring::Mystring(int iter, char lettre)
{
    n=iter;
    tab = new char [n+1];
    cout<<"constructeur 3"<<endl;
    for (int i=0;i<n;i++)
    {
        tab[i]=lettre;
    }
    tab[n+1]='\0';
    stat = new int [26];
    majstat();
}

Mystring& Mystring::operator=(const Mystring& s)
{
    if (this != &s){

        cout<<"DESTRUCTEUR"<<endl;

        if (tab != NULL)
        {
            delete tab;
            delete stat;

        }

        n = s.n;
        tab = new char[n+1];
        strcpy(tab,s.tab);
        caractere_spe = s.caractere_spe;
        stat = new int[26];
        for (int i = 0; i<26; i++)
        {
            stat[i] = s.stat[i];
        }
    }
    return(*this);
}
/*Mystring Mystring::operator+(const Mystring&s)
{
 Mystring res(*this);
 res.concat(caractere_spe);//CA NE MARCHE PAS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 return res;
}*/

Mystring Mystring::operator-(const Mystring&s)
{
 Mystring res(*this);
 res.supprime(caractere_spe);
 return res;
}

ostream& operator<<(ostream&os,Mystring&objet)
{
    os<<"ma chaine est :"<<endl;
    os<<objet.tab<<endl;
    return os;
}



Mystring::~Mystring()
{
    cout<<"DESTRUCTEUR"<<endl;
    delete tab;
    delete stat;
}

void Mystring::affiche()
{
    cout<<"Affichage :"<<endl;
    if (tab!=NULL){
        cout<<"chaine="<<tab<<endl;
    }
    cout<<"n="<<n<<endl;
    cout<<"caracteres_spe="<<caractere_spe<<endl;
    for (int i=0;i<26;i++){
        cout<<stat[i]<<"; ";
    }
    cout<<endl;
}




void Mystring::dedouble(char car)
{
    tab = dedoubler(tab,car);
    majstat();
}

void Mystring::concat(char *s1, char *s2)
{
    tab=concatb(*s1,*s2);
    majstat();
}


void Mystring::supprime(char car)
{
    tab=supprimer(tab,car);
    majstat();
}


int main()
{
    Mystring S1;
    S1.affiche();
    Mystring S2("abc");
    S2.dedouble('b');
    S2.affiche();
    Mystring S3(4,'d');
    S3.affiche();
    //S3.concat(S1,S2);
    //Mystring S1("iut"), S2("info"),S3("salut");
    //S3 = S1 + S2;
    //S3.affiche();


    return 0;
 }
