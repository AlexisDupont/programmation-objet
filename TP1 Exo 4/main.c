#include <stdio.h>
#include <stdlib.h>
int factorielle (int n)
{
    int i;
    float prod;
    prod=1;
    for (i=1; i<=n; i++)
    {
        prod=prod*i;
    }
    return prod;
}

int fcnp(int n, int p)
{
    float prod;
    prod = ((factorielle(n))/((factorielle(n-p))*(factorielle(p))));
    return prod;
}

/*float puissance (float x, int y)
{
    float puiss;
    int i;
    printf("Donnez un nombre: ");
    scanf("%f", &x);
    printf("Donnez la puissance: ");
    scanf("%d", &y);
    puiss=1;
    for(i=1;i<=y;i++)
    {
        puiss=puiss*x;
    }
    return puiss;
}
*/

int main()
{
    int n,p;
    float prod;
    printf("Donnez une valeur de n ");
    scanf ("%d", &n);
    printf("Donnez une valeur de p ");
    scanf ("%d", &p);
    while (p>=n)
    {
        printf("Valeur de p incorrecte \n");
        printf("Donnez une nouvelle valeur de p: ");
        scanf("%d", &p);
    }
    prod = fcnp(n,p);
    printf("Le produit est %f", prod);
    return 0;
}
