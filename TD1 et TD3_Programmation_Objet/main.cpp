#include <iostream>
#include <math.h>
#include <string.h>
#include <vector>

using namespace std;

class point
{
private :
    float x, y ;
    float rho, theta;
    void coordonnees_polaire();
public :
    point();
    point(float);
    point(float, float);
    ~point();
    void affiche();
    void deplace(float,float);
    void init (float,float);
    void homothetie(int);
    void rotation(float);
    float getx(){return x;}
    float gety(){return y;}
};

void point::rotation(float ang)
{
    ang=ang*M_PI/180;
    theta=theta+ang;
    x=rho*cos(theta);
    y=rho*sin(theta);
}
void point::coordonnees_polaire()
{
    rho = sqrt(x*x+y*y);
    theta = atan2(y, x);
    theta=theta*180/M_PI;

}

point::point()
{
    cout<<"constructeur1"<<endl;
    x=y=0;
    coordonnees_polaire();

}

point::point(float p)
{
    cout<<"constructeur2"<<endl;
    x=y=p;
    coordonnees_polaire();

}

point::point(float px, float py)
{
    cout<<"constructeur3"<<endl;
    x=px;
    y=py;
    coordonnees_polaire();

}

point::~point()
{
    cout<<"destructeur"<<endl;
}



void point::affiche()
{
    cout<<"x = "<<x<<endl;
    cout<<"y = "<<y<<endl;
    //cout<<"rho = "<<rho<<endl;
    //cout<<"theta = "<<theta<<endl;
    coordonnees_polaire();

}

void point::deplace(float dx, float dy)
{
    cout<<"deplace: "<<endl;
    x=x+dx;
    y=y+dy;
    cout<<"x = "<<x<<endl;
    cout<<"y = "<<y<<endl;
    coordonnees_polaire();
}
void point::homothetie(int h)
{
    cout<<"homothetie: "<<endl;
    x=x*h;
    y=y*h;
    cout<<"x = "<<x<<endl;
    cout<<"y = "<<y<<endl;
    coordonnees_polaire();
}
void point :: init (float px, float py)
{
    x=px;
    y=py;
    coordonnees_polaire();
}
void f()
{
    point tmp(99);
    tmp.affiche();
}

class rectangle
{
protected :
    point p1, p2;
public :
    rectangle();
    rectangle(float, float, float, float);
    rectangle(point, float, float);
    rectangle(point, point);
    ~rectangle();
    void init(float, float, float, float);
    void affiche();
    bool selection(float, float);
    rectangle(const rectangle&);
    rectangle& operator=(const rectangle&);

};

rectangle::rectangle(): p1(), p2()
{

}

rectangle::rectangle(float px1, float py1, float px2, float py2): p1(px1, py1), p2(px2, py2){ }

rectangle::rectangle(point pp, float pl, float ph):p1(pp), p2(pp.getx() + pl, pp.gety() + ph){ }

rectangle::rectangle(point pp1, point pp2):p1(pp1), p2(pp2){ }

rectangle::~rectangle(){ }

rectangle::rectangle(const rectangle&s): p1(s.p1), p2(s.p2){ }

rectangle& rectangle::operator=(const rectangle&s)
{
    p1 = s.p1;
    p2 = s.p2;
    return *this;
}

void rectangle::init(float px1, float py1, float px2, float py2)
{
    p1.init(px1, py1);
    p2.init(px2, py2);
}

void rectangle::affiche()
{
    cout<<"objet rectangle-"<<"adresse ="<<this<<endl;
    p1.affiche();
    p2.affiche();
}

bool rectangle::selection(float sx, float sy)
{
    if((sx >= p1.getx()) && (sx <= p2.getx()) && (sy >= p1.gety()) && (sy <= p2.gety()))
    {
        return true;
    }
    return false;
}

class bouton
{
protected:
    rectangle r;
    string ch;
public:
    bouton();
    bouton(float, float, char*);
    bouton(point, point, char*);
    bouton(point, float, float);
    bouton(point, float, float, char*);
    bouton(float, float, float, float, char*);
    bouton(rectangle, char*);
    ~bouton();
    bouton(const bouton&);
    bouton& operator=(const bouton&);
    void init(float, float, float, float, char*);
    void affiche();
};

bouton::bouton():r(), ch(){ }

bouton::bouton(float px, float py, char* pch): r(point(px, py), strlen(pch)+10,20), ch(pch){ }

bouton::bouton(float px1, float py1, float px2, float py2, char* pch): r(px1, py1, px2, py2), ch(pch){ }

bouton::bouton(point pp, float pl, float ph, char* pch): r(pp, pl, ph), ch(pch){ }

bouton::bouton(point pp1, point pp2, char* pch):r(pp1, pp2), ch(pch){ }

bouton::bouton(rectangle pr, char* pch):r(pr), ch(pch){ }

bouton::bouton(const bouton&s):r(s.r), ch(s.ch){ }

bouton::~bouton(){ }

bouton& bouton::operator=(const bouton&s)
{
    ch = s.ch;
    r = s.r;
    return *this;
}

void bouton::init(float px1, float py1, float px2, float py2, char* pt)
{
    r.init(px1, py1, px2, py2);
    string tmp(pt);
    ch = tmp;
}

void bouton::affiche()
{
    r.affiche();
    cout<<"chaine = "<<ch<<endl;
    cout<<"taille = "<<ch.length()<<endl;
}


class menu
{
protected:
    bouton *tab;
    int n;
public:
    menu(char*[], int);
    ~menu();
    menu(const menu&);
    menu& operator=(const menu&);
    void affiche();
};

menu::menu(char *t[], int pn)
{
    n = pn;
    tab = new bouton[n];
    for(int i=0; i<n; i++)
    {
        tab[i].init(2*i,2*i,5*i,5*i,t[i]);
    }
}

menu::~menu()
{
    delete []tab;
}

menu::menu(const menu& s)
{
    tab = new bouton[n = s.n];
    for(int i=0; i<n; i++)
    {
        tab[i] = s.tab[i];
    }
}

menu& menu::operator=(const menu&s)
{
    if(this != &s)
    {
        delete []tab;
        tab = new bouton[n = s.n];
        for(int i=0; i<n;i++)
        {
            tab[i] = s.tab[i];
        }
    }
    return *this;
}

void menu::affiche()
{
    cout<<"menu :"<<endl;
    for(int i=0; i<n; i++)
    {
        tab[i].affiche();
    }
}


class menubis
{
protected:
    bouton **tab;
    int n;
public:
    menubis(char*[], int);
    ~menubis();
    menubis(const menubis&);
    menubis& operator=(const menubis&);
    void affiche();
};

menubis::menubis(char*t[], int pn)
{
    n = pn;
    tab = new bouton*[n];
    for(int i=0; i<n; i++)
    {
        tab[i] = new bouton(10*i,5*i,t[i]);
    }
}

menubis::~menubis()
{
    for(int i=0; i<n; i++)
    {
        delete tab[i];
    }
    delete tab;
}

menubis::menubis(const menubis& s)
{
    tab = new bouton*[n = s.n];
    for(int i=0; i<n; i++)
    {
        tab[i] = new bouton(*(s.tab[i]));
    }
}

menubis& menubis::operator=(const menubis&s)
{
    if(this != &s)
    {
        for(int i=0; i<n; i++)
        {
            delete tab[i];
        }
        delete tab;
        tab = new bouton*[n = s.n];
        for(int i=0; i<n; i++)
        {
            tab[i] = new bouton(*(s.tab[i]));
        }
    }
    return *this;
}

void menubis::affiche()
{
    cout<<"menubis :"<<endl;
    for(int i=0; i<n; i++)
    {
        tab[i]->affiche();
    }
}

class menuter
{
protected:
    vector <bouton> *tab;
    int n;
public:
    menuter(char*[], int);
    ~menuter();
    menuter(const menuter&);
    menuter& operator=(const menuter&);
    void affiche();
};

menuter::menuter(char*t[], int pn)
{
    n = pn;
    tab = new vector <bouton>;
    for(int i=0; i<n; i++)
    {
        tab->push_back(bouton(10*i,5*i,t[i]));
    }
}

menuter::~menuter()
{
    delete tab;
}

menuter::menuter(const menuter &s)
{
    n = s.n;
    tab = new vector <bouton>(n);
    for(int i=0; i<n; i++)
    {
        (*tab)[i] = (*s.tab)[i];
    }
}

menuter&menuter::operator=(const menuter&s)
{
    if(this != &s)
    {
        delete tab;
        n = s.n;
        tab = new vector <bouton>(n);
        for(int i=0; i<n; i++)
        {
            (*tab)[i] = (*s.tab)[i];
        }
    }
    return *this;
}

void menuter::affiche()
{
    cout<<"menuter :"<<endl;
    for(int i=0; i<n; i++)
    {
        (*tab)[i].affiche();
    }
}

int main()
{
    /*point a;
    a.affiche();
    a.deplace(3,3);
    a.homothetie(2);
    a.rotation(90);
    a.affiche();

    point b(5);
    c.homothetie(2);
    c.rotation(90);
    c.affiche();
    cout<<"appel f"<<endl;
    f();
    cout<<"fin f"<<endl;
    point *bd = new point(100, 400);
    (*bd).affiche();
    delete bd;

    rectangle r1(10,20,50,60);
    r1.affiche();
    rectangle r2(point(50,60),100,120);
    r2.affiche();
    rectangle r3(point(50,60), point(150,160));
    r3.affiche();
    rectangle r3bis(r3);
    r3bis.affiche();
    r2 = r1;
    r2.affiche();

    char tmp[] = "labelbouton";
    bouton b1(10,20,50,60,tmp);
    b1.affiche();
    bouton b2(point(50,60),100,120,tmp);
    b2.affiche();
    bouton b3(point(50,60),point(150,160),tmp);
    b3.affiche();
    bouton b3bis(b3);
    b3bis.affiche();
    b2 = b1;
    b2.affiche();

    char *t[] = {"bton1", "bton2", "bton3", "bton4"};
    menu m1(t,4);
    m1.affiche();
    menu m2(t,2);
    m2.affiche();
    menu m3(m2);
    m3.affiche();
    m2 = m1;
    m2.affiche();

    char *t[] = {"bt1", "bt2", "bt3", "bt4"};
    menubis m1(t,4);
    m1.affiche();
    menubis m2(t,2);
    m2.affiche();
    menubis m3(m2);
    m3.affiche();
    m2 = m1;
    m2.affiche();*/

    char *t[] = {"bton1", "bton2", "bton3", "bton4"};
    menuter m1(t,4);
    m1.affiche();
    menuter m2(t,2);
    m2.affiche();
    menuter m3(m2);
    m3.affiche();
    m1 = m1;
    m2.affiche();
    return 0;
}
